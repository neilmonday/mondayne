#!/usr/bin/env python3

import sys, os, subprocess, configparser
import daemon

if __name__ == "__main__":

    config = configparser.ConfigParser()
    config.read("config.ini")
    log_directory = config.get("Directories", "log")  #ConfigSectionMap("Directories", config)['log']

    mydaemon = daemon.Daemon('/tmp/mondayne.pid', '/dev/null', log_directory + 'output.log', log_directory + 'error.log', os.getcwd())
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            mydaemon.start()
        elif 'stop' == sys.argv[1]:
            mydaemon.stop()
        elif 'restart' == sys.argv[1]:
            mydaemon.restart()
        else:
            print("Unknown Command.")
            sys.exit(2)
        sys.exit(0)
    else:
        print("usage: %s start|stop|restart" % sys.argv[0])
        sys.exit(2)
#end of main()
